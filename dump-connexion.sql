-- MariaDB dump 10.19  Distrib 10.6.7-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: connexion
-- ------------------------------------------------------
-- Server version	10.6.7-MariaDB-2ubuntu1.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `connexion`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `connexion` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `connexion`;

--
-- Table structure for table `formu`
--

DROP TABLE IF EXISTS `formu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formu`
--

LOCK TABLES `formu` WRITE;
/*!40000 ALTER TABLE `formu` DISABLE KEYS */;
INSERT INTO `formu` VALUES (1,'zahra@gmail.com','zahra'),(2,'mimi@gmail.com','123456789'),(3,'chantal@gmail.com','chantal'),(4,'majid@gmail.com','987654321'),(5,'toto@gmail.com','tototo'),(6,'titi@gmail.com','titititi'),(7,'titi@gmail.com','titititi'),(8,'paul@gmail.com','paulpaul'),(9,'Nico@gmail.com','$2y$10$.9BxP3SQbbs8kibUaW61aOy6VIwEPUsKg.mhQ5uHCzN.7U8me6YhW'),(10,'nini@gmail.com','$2y$10$p.Qn4.g4xxbZ7UkW/E5d4eDkTKNHD9yEcWFK.M4HFFTLn9I5JCEh.'),(11,'nana@gmail.com','$2y$10$8cwQ/NLnsyITTbtGf2cUIOVwbx8LXa.pC7Gzjr8z2r00H44cY3eOy'),(12,'nono@gmail.com','$2y$10$BoBnNGml5easkF.lwH56seiMLbz8NNcz43wZAjB52F8p0YL4vvt8G'),(13,'nyny@gmail.com','$2y$10$a/3Ojkn0PHAKAzpzdYOzTuAmaB2rZan69kBMjscyXzMJT0CyRJ4WW'),(14,'zahra@gmail.com','$2y$10$943T6YG4MRVwPy0pcnnQ0uUVjRhzcwD/TIXmMp7znznMKKbiZQbAi'),(15,'zahra@gmail.com','$2y$10$76eh8L5nO2km6MME9mwOj.QsFcmfRIVNSptowQjR2gLEyu.jJbiXS'),(16,'nano@hotmail.com','$2y$10$potiRqMfo8sFPc315fZpYOPpcDjBPZEazJoRHv1ywlrcLVxilzOzC'),(17,'nano@hotmail.com','$2y$10$tSH6YeBwxgE2nZ2ud08MJeACis.xcMXfWolmEJWqhYQRYXI6rrAaC'),(18,'','$2y$10$tIqiSm71zXo86jJAH6lEhuw27KLkf4M44TPGvTRYRiHrIu18GCWs2'),(19,'','$2y$10$T2uH84TRDtRwY6qVDE93wO30VtcSG4W0FyRtFbJAR9pzV9aC7zQza'),(20,'','$2y$10$rVtldckP7XsGLtLawQxZGemwU.A3XqU2Dto07etwIMD2aqnKmkPCi'),(21,'','$2y$10$zOq4A.mD/GiY8Jzk.eIBMuyklx9VSfc.M0GpM44dyxu7G30dF50.G'),(22,'','$2y$10$oce64peKk4ub3He/ANiWq.E53RFlNB/vRumGN3I6A35W.sXD6rGTG'),(23,'','$2y$10$sBEkh4Hs.AuYJ9k0rz2CeO0lIAnezFcusaP3QmEl0/xZCZQskNwXS'),(24,'zaza@hotmail.com','$2y$10$x.d0VORi2aujnC18dqObEuD8EOQCide0pUPrAbXqr4W62goJZ4W0m');
/*!40000 ALTER TABLE `formu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-25 17:25:23
