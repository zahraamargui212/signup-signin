<?php

require_once 'bd.php';
// require_once 'connexion.php';

require_once 'session.php';

 
   
// supprime les variables de sessions
session_unset(); 
// détruit la session 
session_destroy();
// indique au navigateur qu'il faut supprimer le cookie de session
setcookie(session_name(), '', strtotime('-1 day'));

header('Location: connexion.php');
