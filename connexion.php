<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once 'session.php';
//  require_once 'inscription.php';

if(isset($_SESSION["msg"])) {
    echo $_SESSION["msg"];
}

 require_once 'bd.php';
 


?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>form_validation</title>
    <link rel="stylesheet" href="style.css">
</head>

<body>




    <!--Main wrapper-->
    <div class="wrapper">
        <h1>Connexion</h1>

        <!--form container-->
        <div class="form-container">
            <form novalidate action="" method="post">
                <!--flexbox and it's items-->
                <div class="flex">
                    <div class="flex-item">
                        <!--Pseudo field-->
                       <!-- <div class="field-container">
                            <label for="name">name: <span class="required">*</span></label>
                            <input type="text" name="name" pattern="^([a-zA-Z]{2,} ?)+$" id="name"
                                placeholder="Your pseudo" required="required" />
                            <span class="error-messg"></span>
                        </div> -->
                    
                        <!--email field-->
                        <div class="field-container">
                            <label for="email">Email: <span class="required">*</span></label>
                            <input type="email" name="email" id="email" placeholder="Your email" required="required" />
                            <span class="error-messg"></span>
                        </div>

                        

                    </div>
                    <div class="flex-item">
                        

                        <!--password field-->
                        <div class="field-container">
                            <label for="passkey">Password: <span class="required">*</span></label>
                            <div class="passkey-box">
                                <input type="password" name="passkey" id="passkey" class="passkey"
                                    placeholder="Password" required="required" />
                                    <p style="display:none" id ="error" >Non d'utilisateur ou mot de passe Icorrect !!</p>
                                <span class="passkey-icon" data-display-passkey="off"><i class="fas fa-eye"></i> </span>
                            </div>
                            <span class="error-messg"></span>
                        </div>
                      
                        <!--confirm password field-->
                          <!--
                        <div class="field-container">
                            <label for="confirm-passkey">Confirm password: <span class="required">*</span></label>
                            <div class="passkey-box">
                                <input type="password" name="confirm-passkey" class="passkey" id="confirm-passkey"
                                    placeholder="Re-enter password" required="required" />
                                <span class="passkey-icon" data-display-passkey="off"><i class="fas fa-eye"></i></span>
                            </div>
                            <span class="error-messg"></span>
                        </div> 
                        -->

                    </div>
                </div>
                <!--Submit button & CGU-->

                <div class="center"><input type="submit" name="ok" value="Connexion"></div>

                <!--deconnexion-->

               
            </form>
        </div>




    </div>
    </form>
    </div>

    </div>
   <!-- <script src="script.js"></script>-->
    <?php
    if(isset($_POST['ok'])){
        if (!empty($_POST['passkey'])  && !empty($_POST['email'])) {
       
        $search_html = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL); 
        $pass = $_POST['passkey'];

        $email = htmlspecialchars($_POST['email']);
        $email = trim($email);  

        $sql ="SELECT * FROM formu WHERE email='$email' AND password ='$pass' ";
        $reponse = $db->query($sql);
        $user = $reponse->fetch();
        if ($user) {
            $_SESSION['username'] = $user['email'];
           header('Location:profil.php');
            
        } else {
            echo "infos de connexion invalides ";
            
        }
        // print_r($reponse->rowCount()) ;
        // header('Location:deconnexion.php');
    }
    }

    
    ?>

</body>

</html>


