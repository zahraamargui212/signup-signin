<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();

require_once 'bd.php';




if (isset($_POST['submit'])) {

    // validation email
    $search_html = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL); 

    // sécurisation email
    $search_html = htmlspecialchars($search_html);

    // vérifier si l'email existe en BDD (SELECT)
    $sql = "SELECT * FROM formu WHERE email = :email";

    $stm = $db->prepare($sql);

    $data = [
        ':email' => $search_html
    ];

    $stm->execute($data);

    $user = $stm->fetch();


    // si l'email n'existe pas => inscription
    // sinon msg d'erreur

        
    //var_dump($user);
    //exit;

    /// hashage de mot de password
    $pass = $_POST['passkey'];
    // on verifie que password et Confirm password sont identique
    $pass1 = $_POST["confirm-passkey"];
    if  (!empty($_POST['passkey'])  && !empty($_POST['confirm-passkey'])) {
        # code...
    
    if ($_POST["passkey"] == $_POST["confirm-passkey"]) {
        $hashed_pass = password_hash($pass, PASSWORD_DEFAULT);
        // $search_html = filter_input(INPUT_POST,'email',FILTER_VALIDATE_EMAIL); 
        
        $data = [
            ':email' => $search_html,
            ':password' => $hashed_pass,
        ];
        try {
            $sql = "INSERT INTO formu (email,password) VALUES (:email,:password)";
            
          $stm = $db->prepare($sql);
            $stm->execute($data);
           
        } catch (PDOException $e) {
            if($e->errorInfo[1] === 1062) {
              //  if($e->code === '23000')
                $_SESSION["msg"] = "Cet email existe déjà";
                header("Location:connexion.php");
                
            }
            
        }
        $_SESSION["msg"] = "Utilisateur créé";
        header("Location:connexion.php");

    } else {
        $_SESSION["msg"] = "Les mots de passe ne sont pas identiques";
        header("Location:index.php");
}

    
} else {
    header('Location:index.php');
}
}

//si l'email existe ou pas

